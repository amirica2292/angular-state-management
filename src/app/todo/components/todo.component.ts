import { Component, OnInit } from '@angular/core';
import { Store, select, Action } from '@ngrx/store';
import { Observable } from 'rxjs';

import { ToDoState } from '../store/todo.state';
import { CREATE_TODO, GET_TODO, GetToDo, CreateToDo } from '../store/todo.action';
import ToDo from '../todo.model';
import ActionWithPayload from '../ActionWithPayload';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  ToDoState$: Observable<ToDoState>;
  title: string;
  completed: boolean = false;
  ToDoList: ToDo[];
  
  constructor(private store: Store<ToDoState>) { }

  ngOnInit() {
    this.ToDoState$ = this.store.pipe(select('todos'));    
    this.ToDoState$.pipe(map(x => this.ToDoList = x.ToDoList)).subscribe();
    this.store.dispatch(new GetToDo());
  }

  createToDo() {
    this.store.dispatch(new CreateToDo(<ToDo> { title: this.title, isCompleted: this.completed }));
  }
}