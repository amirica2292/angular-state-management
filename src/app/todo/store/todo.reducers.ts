import ActionWithPayload from "../ActionWithPayload";
import ToDo from "../todo.model";
import { ToDoState, initializeState } from "./todo.state";
import * as ToDoActions from "./todo.action";
import { Action } from "@ngrx/store";

const initialState = initializeState();

export function ToDoReducer(state: ToDoState = initialState,
    action: Action) {

    switch (action.type) {
        case ToDoActions.GET_TODO:
            return { ...state, Loaded: false, Loading: false };

        case ToDoActions.CREATE_TODO:
            return ({
                ...state,
                Loading: true, Loaded: false
            });

        case ToDoActions.GET_TODO_SUCCESS:
            return ({
                ...state,
                ToDoList: state.ToDoList.concat((action as ActionWithPayload<ToDo[]>).payload),
                Loading: false, Loaded: true
            });

        case ToDoActions.CREATE_TODO_SUCCESS:
            return ({
                ...state,
                ToDoList: [...state.ToDoList, (action as ActionWithPayload<ToDo>).payload],
                Loading: false, Loaded: true
            });

        case ToDoActions.GET_TODO_ERROR:
            return ({
                ...state,
                Loading: false, Loaded: false, 
            });

        default:
            return state;
    }
}